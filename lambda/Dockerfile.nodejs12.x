# Buildimage for Lambda
# See: https://github.com/aws/aws-sam-cli/blob/develop/build-image-src/Dockerfile-nodejs12x
FROM amazon/aws-sam-cli-emulation-image-nodejs12.x

# To learn more context around use of `amazonlinux:2` image please read comment in java11/build/Dockerfile
# Copying root from runtimes image to AL2
FROM amazonlinux:2
COPY --from=0 / /rootfs

ENV PATH=/var/lang/bin:$PATH \
  LD_LIBRARY_PATH=/var/lang/lib:$LD_LIBRARY_PATH \
  AWS_EXECUTION_ENV=AWS_Lambda_nodejs12.x \
  NODE_PATH=/opt/nodejs/node12/node_modules:/opt/nodejs/node_modules:/var/runtime/node_modules

# Installing by yum at copied location
RUN yum groupinstall -y development --installroot=/rootfs && \
  yum install -d1 --installroot=/rootfs -y \
  tar \
  gzip \
  unzip \
  jq \
  grep \
  curl \
  make \
  binutils \
  gcc-c++ \
  procps \
  libgmp3-dev \
  zlib1g-dev \
  libmpc-devel \
  && yum clean all

# Copying root from AL2 to runtimes image
FROM amazon/aws-sam-cli-emulation-image-nodejs12.x
COPY --from=1 /rootfs /

# Install yarn
# Cannot use >1.22.5, see https://github.com/yarnpkg/yarn/issues/8358
ENV YARN_VERSION 1.22.5

RUN set -ex \
  && npm i -g "yarn@$YARN_VERSION" \
  && yarn --version
