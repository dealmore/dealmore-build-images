#!/bin/bash

DATA_DIR="/data"

# Check user environment variable
if [[ -z "${MINIO_CREATE_BUCKET}" ]]; then
  printf "Missing MINIO_CREATE_BUCKET environment variable\n";
  printf "No buckets are created\n";
else
  printf "Initialize Buckets\n";

  # Create buckets
  # https://stackoverflow.com/a/15400047/831465
  set -f; # avoid globbing (expansion of *).
  buckets=(${MINIO_CREATE_BUCKET//,/ });

  for i in "${!buckets[@]}"
  do
    printf "Initialized Bucket: ${buckets[i]}\n";
    mkdir -p "${DATA_DIR}/${buckets[i]}";
  done
fi

# Start server from original entrypoint
source /usr/bin/docker-entrypoint.sh server "${DATA_DIR}"
