#!/bin/sh

# Generates a .netrc file
FILE="/root/.netrc"

/bin/cat <<EOM >$FILE
machine api.heroku.com
  password ${2}
  login ${1}
machine git.heroku.com
  password ${2}
  login ${1}
EOM

chmod 600 $FILE