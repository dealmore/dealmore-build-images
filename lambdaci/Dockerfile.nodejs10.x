FROM lambci/lambda:build-nodejs10.x

# Since yum is not capable of installing yarn without nodejs
# dependency we use yarn from npm
# Would be great if someone fixes this by using rpm https://serverfault.com/a/681848/278481
RUN npm i -g yarn
