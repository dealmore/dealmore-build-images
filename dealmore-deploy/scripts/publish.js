'use strict';

/**
 * Publishes the artifacts of the current build to S3
 */

const path = require('path');
const fs = require('fs');
const argv = require('yargs').argv;
const glob = require('glob');
const AWS = require('aws-sdk');
const mime = require('mime-types');

const getS3Config = require('./utils/s3-config');

const BUILD_DIR = '__build__';
const S3_ACCESS_KEY_ID = argv.S3_ACCESS_KEY_ID;
const S3_SECRET_ACCESS_KEY = argv.S3_SECRET_ACCESS_KEY;
const S3_BUCKET = argv.S3_BUCKET;
const BUILD_TAG = argv.TAG;
const TARGET = argv.TARGET;

const buildPath = `${BUILD_DIR}/**/*.*`;

const s3 = new AWS.S3(
  getS3Config(TARGET, {
    S3_ACCESS_KEY_ID: S3_ACCESS_KEY_ID,
    S3_SECRET_ACCESS_KEY: S3_SECRET_ACCESS_KEY,
  })
);

glob(buildPath, function(err, files) {
  if (err) {
    throw err;
  }

  files.forEach(fileName => {
    const filePath = path.resolve(__dirname, '..', fileName);
    const key = fileName.slice(BUILD_DIR.length + 1);
    const file = fs.readFileSync(filePath);
    const contentType = mime.lookup(key);

    // Push artifacts to S3
    const params = {
      Body: file,
      Bucket: S3_BUCKET,
      Key: `${BUILD_TAG}/${key}`,
      ContentType: contentType,
    };
    s3.putObject(params, err => {
      if (err) {
        throw err;
      }

      console.log('Pushed to S3: ', params.Key);
    });
  });
});

return 0;
