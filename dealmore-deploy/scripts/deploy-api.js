'use strict';

/**
 * Deploys a new version of the API to the ecs service
 */
const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
const argv = require('yargs').argv;

const IMAGE_URL = 'registry.gitlab.com/dealmore/dealmore-api';

const {
  TAG,
  DM_BASE_URL,
  DM_CDN_BASE_URL,
  DM_CDN_DIST_URL,
  DM_MONGODB_USER,
  DM_MONGODB_PASS,
  DM_AWS_ACCESS_KEY_ID,
  DM_AWS_SECRET_ACCESS_KEY,
  DM_S3_BUCKET,
  DM_S3_REGION,
  DEPLOY_REGION,
  DEPLOY_ACCESS_KEY_ID,
  DEPLOY_SECRET_ACCESS_KEY,
  DEPLOY_SERVICE,
  DEPLOY_CLUSTER,
  PORT,
  DM_CLOUDWATCH_GROUP,
} = argv;

const ecs = new AWS.ECS({
  apiVersion: '2014-11-13',
  region: DEPLOY_REGION,
  accessKeyId: DEPLOY_ACCESS_KEY_ID,
  secretAccessKey: DEPLOY_SECRET_ACCESS_KEY,
});
const environment = {
  APP_VERSION: TAG,
  DM_BASE_URL: DM_BASE_URL,
  DM_CDN_BASE_URL: DM_CDN_BASE_URL,
  DM_CDN_DIST_URL: DM_CDN_DIST_URL,
  DM_MONGODB_USER: DM_MONGODB_USER,
  DM_MONGODB_PASS: DM_MONGODB_PASS,
  DM_AWS_ACCESS_KEY_ID: DM_AWS_ACCESS_KEY_ID,
  DM_AWS_SECRET_ACCESS_KEY: DM_AWS_SECRET_ACCESS_KEY,
  DM_S3_BUCKET: DM_S3_BUCKET,
  DM_S3_REGION: DM_S3_REGION,
  PORT: PORT,
  DM_CLOUDWATCH_GROUP: DM_CLOUDWATCH_GROUP,
};

function generateEnvironment() {
  return Object.keys(environment).map(key => ({
    name: key,
    value: environment[key].toString(),
  }));
}

/**
 * Generates a new task-revision
 */
function generateTaskRevision(templateFile) {
  // Read base file
  const filePath = path.resolve(__dirname, 'templates', templateFile);
  const file = fs.readFileSync(filePath);
  const params = JSON.parse(file);
  const environment = generateEnvironment();

  params.containerDefinitions[0].environment = environment;
  params.containerDefinitions[0].image = `${IMAGE_URL}:${TAG}`;

  return params;
}

function registerTaskDefinition() {
  return new Promise((resolve, reject) => {
    const params = generateTaskRevision('start-dealmore-api.json');
    ecs.registerTaskDefinition(params, function(err, data) {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
}

// Gets the current running service
function getService(serviceName) {
  return new Promise((resolve, reject) => {
    const params = {
      services: [serviceName],
      cluster: DEPLOY_CLUSTER,
    };
    ecs.describeServices(params, (err, data) => {
      if (err) {
        return reject(err);
      }

      resolve(data.services[0]);
    });
  });
}

function updateService(data) {
  return getService(DEPLOY_SERVICE).then(
    service =>
      new Promise((resolve, reject) => {
        const { family, revision } = data.taskDefinition;
        const filePath = path.resolve(__dirname, 'templates', 'dealmore-api-service.json');
        const file = fs.readFileSync(filePath);
        const defaultParams = JSON.parse(file);

        var params = Object.assign(
          {
            desiredCount: service.desiredCount,
            service: DEPLOY_SERVICE,
            taskDefinition: `${family}:${revision}`,
            cluster: DEPLOY_CLUSTER,
          },
          defaultParams
        );
        ecs.updateService(params, function(err, data) {
          if (err) {
            return reject(err);
          }
          resolve(data);
        });
      })
  );
}

Promise.resolve()
  .then(registerTaskDefinition)
  .then(updateService)
  .catch(err => {
    console.log(err);
    process.exit(1);
  });
