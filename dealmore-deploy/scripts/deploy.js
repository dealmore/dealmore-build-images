'use strict';

/**
 * Deploys the app to the given environment
 */

const argv = require('yargs').argv;
const AWS = require('aws-sdk');
const request = require('request');

const getS3Config = require('./utils/s3-config');

const ENV_URL = argv.ENV_URL;
const BUILD_TAG = argv.TAG;
const S3_ACCESS_KEY_ID = argv.S3_ACCESS_KEY_ID;
const S3_SECRET_ACCESS_KEY = argv.S3_SECRET_ACCESS_KEY;
const S3_BUCKET = argv.S3_BUCKET;
const TARGET = argv.TARGET;
const PUSH_SECRET = argv.PUSH_SECRET;

const SITE_CONFIG_KEY = 'site-config.json';
const PUSH_URL = 'api/platform/push';

/**
 * Fetches the site-config from the S3 bucket
 */
const getSiteConfig = () =>
  new Promise((resolve, reject) => {
    const s3 = new AWS.S3(
      getS3Config(TARGET, {
        S3_ACCESS_KEY_ID: S3_ACCESS_KEY_ID,
        S3_SECRET_ACCESS_KEY: S3_SECRET_ACCESS_KEY,
      })
    );

    const params = {
      Bucket: S3_BUCKET,
      Key: `${BUILD_TAG}/${SITE_CONFIG_KEY}`,
    };
    s3.getObject(params, function(err, data) {
      if (err) {
        return reject(err);
      }

      try {
        const config = JSON.parse(data.Body);
        resolve(config);
      } catch (err) {
        reject(err);
      }
    });
  });

const depolyToServer = siteConfig =>
  new Promise((resolve, reject) => {
    console.log(`Pushing to ${ENV_URL}...`);
    request.post(
      {
        url: `${ENV_URL}/${PUSH_URL}`,
        body: { siteConfig: siteConfig, token: PUSH_SECRET, buildTag: BUILD_TAG },
        json: true,
        rejectUnauthorized: false,
      },
      function(error, response) {
        if (error) {
          if (response) {
            console.log(response);
          }
          return reject(error);
        }

        if (response.statusCode === 201) {
          console.log(`Push to ${ENV_URL} successful!`);
          return resolve();
        }
        reject(response);
      }
    );
  });

function run() {
  Promise.resolve()
    .then(getSiteConfig)
    .then(depolyToServer)
    .then(() => process.exit(0))
    .catch(err => {
      console.log(err);
      process.exit(1);
    });
}

run();
