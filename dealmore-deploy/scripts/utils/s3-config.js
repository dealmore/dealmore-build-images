'use strict';

/**
 * Generates the S3 config for a target
 */

const AWS = require('aws-sdk');

const getS3Config = (target, options) => {
  // The local option matches the fake-s3
  if (target === 'local') {
    const config = {
      host: '127.0.0.1',
      port: '4569',
      secretAccessKey: '0000',
      accessKeyId: '0000',
      region: 'eu-central-1',
      apiVersion: '2006-03-01',
      sslEnabled: false,
      s3ForcePathStyle: true,
    };

    config.endpoint = new AWS.Endpoint(`http://${config.host}:${config.port}`);
    return config;
  }

  return {
    secretAccessKey: options.S3_SECRET_ACCESS_KEY,
    accessKeyId: options.S3_ACCESS_KEY_ID,
    region: 'eu-central-1',
    apiVersion: '2006-03-01',
  };
};

module.exports = getS3Config;
