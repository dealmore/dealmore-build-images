# Dealmore build images

Contains the recipes for building docker images we use to build / test / deploy our systems.

## Active images

- `dealmore-node`
- `lambdaci:nodejs12.x`
- `firestore-emulator`
- `firebase-tools`
- `s3-emulator`
