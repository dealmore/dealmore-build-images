#!/bin/sh

# Generate the $SUBIMAGE_NAME and $SUBIMAGE_TAG variables from a split from the
# $CI_JOB_NAME variable
# https://unix.stackexchange.com/q/417187/219972

case $CI_JOB_NAME in
  (*:*) SUBIMAGE_NAME=${CI_JOB_NAME%:*} SUBIMAGE_TAG=${CI_JOB_NAME##*:};;
esac
